import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ProductApiService } from "../../core/services/product.service";
import { Product } from "../../core/models/Product";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"]
})
export class AddProductComponent implements OnInit {
  pageName = "Add product";

  constructor(private productApiService: ProductApiService, private router: Router) {}

  ngOnInit() {}

  onPostNewProduct(product: Product): void {
    this.productApiService.addProduct(product).subscribe(
      result => {
        alert("Product successfully created");
        this.router.navigate(["/shop"]);
      },
      error => {
        alert(
          `${error.message}\nStatus: ${error.status}\nStatus text: ${
            error.statusText
          }`
        );
        console.log(error);
      }
    );
  }
}
