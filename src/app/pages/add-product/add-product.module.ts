import { NgModule } from "@angular/core";

import { AddEditSharedModule } from 'src/app/shared/modules/add-edit-shared.module';
import { AddProductRoutingModule } from "./add-product-routing.module";
import { AddProductComponent } from "./add-product.component";

@NgModule({
  imports: [AddEditSharedModule, AddProductRoutingModule],
  declarations: [AddProductComponent]
})
export class AddProductModule {}
