import { NgModule } from "@angular/core";

import { MainSharedModule } from 'src/app/shared/modules/main-shared.module';
import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';

@NgModule({
  imports: [MainSharedModule, CartRoutingModule],
  declarations: [CartComponent]
})
export class CartModule {}
