import { NgModule } from "@angular/core";

import { MainSharedModule } from 'src/app/shared/modules/main-shared.module';
import { ShopRoutingModule } from "./shop-routing.module";
import { ShopComponent } from "./shop.component";

@NgModule({
  imports: [MainSharedModule, ShopRoutingModule],
  declarations: [ShopComponent]
})
export class ShopModule {}
