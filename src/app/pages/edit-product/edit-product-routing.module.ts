import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { EditProductComponent } from "./edit-product.component";
import { EditProductResolverService } from "./edit-product-resolver.service";

const routes: Routes = [
  {
    path: ":id",
    component: EditProductComponent,
    resolve: {
      product: EditProductResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditProductRoutingModule {}
