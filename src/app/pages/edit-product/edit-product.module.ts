import { NgModule } from "@angular/core";

import { AddEditSharedModule } from "src/app/shared/modules/add-edit-shared.module";
import { EditProductRoutingModule } from "./edit-product-routing.module";
import { EditProductComponent } from "./edit-product.component";

@NgModule({
  imports: [AddEditSharedModule, EditProductRoutingModule],
  declarations: [EditProductComponent]
})
export class EditProductModule {}
