import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { ProductApiService } from "../../core/services/product.service";
import { Product } from '../../core/models/Product';

@Component({
  selector: "app-edit-product",
  templateUrl: "./edit-product.component.html",
  styleUrls: ["./edit-product.component.scss"]
})
export class EditProductComponent implements OnInit {
  pageName = "Edit product";
  selectedProduct: Product;

  constructor(
    private productApiService: ProductApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    // this.getProductFromRoute();
    this.selectedProduct = this.activatedRoute.snapshot.data.product;
  }

  // getProductFromRoute(): void {
  //   this.activatedRoute.data.subscribe((data: { product: Product }) => {
  //     this.selectedProduct = data.product;
  //   });
  // }

  updateProduct(product: Product): void {
    this.productApiService.updateProduct(product).subscribe(
      result => {
        alert("Product successfully updated");
        this.router.navigate(["/shop"]);
      },
      error => {
        alert(
          `${error.message}\nStatus: ${error.status}\nStatus text: ${
            error.statusText
          }`
        );
        console.log(error);
      }
    );
  }

  deleteProduct(product: Product): void {
    this.productApiService.deleteProduct(product).subscribe(
      result => {
        alert("Product successfully deleted");
        this.router.navigate(["/shop"]);
      },
      error => {
        alert(
          `${error.message}\nStatus: ${error.status}\nStatus text: ${
            error.statusText
          }`
        );
        console.log(error);
      }
    );
  }
}
