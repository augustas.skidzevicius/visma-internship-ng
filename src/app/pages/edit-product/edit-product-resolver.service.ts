import { Injectable } from "@angular/core";
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from "@angular/router";
import { Observable, of, EMPTY } from "rxjs";
import { mergeMap } from "rxjs/operators";

import { ProductApiService } from "src/app/core/services/product.service";
import { Product } from "../../core/models/Product";

@Injectable({
  providedIn: "root"
})
export class EditProductResolverService implements Resolve<Product> {
  constructor(private productApiService: ProductApiService, private router: Router) {}

  resolve(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> | Observable<never> {
    const id = Number(router.paramMap.get("id"));

    return this.productApiService.getProduct(id).pipe(
      mergeMap(product => {
        if (product[0]) {
          return of(product[0]);
        } else {
          this.router.navigate(["/shop"]);
          return EMPTY;
        }
      })
    );
  }
}
