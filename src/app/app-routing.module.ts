import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { NotFoundComponent } from "./pages/not-found/not-found.component";

const routes: Routes = [
  { path: "", redirectTo: "shop", pathMatch: "full" },
  { path: "shop", loadChildren: () => import('./pages/shop/shop.module').then(mod => mod.ShopModule) },
  { path: "cart", loadChildren: () => import('./pages/cart/cart.module').then(mod => mod.CartModule) },
  { path: "add-product", loadChildren: () => import('./pages/add-product/add-product.module').then(mod => mod.AddProductModule) },
  { path: "edit-product", loadChildren: () => import('./pages/edit-product/edit-product.module').then(mod => mod.EditProductModule) },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
