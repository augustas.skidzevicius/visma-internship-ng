import { Component, OnInit } from "@angular/core";
import { fromEvent } from "rxjs";
import { debounceTime, switchMap, distinctUntilChanged, merge } from "rxjs/operators";

import { ProductApiService } from "src/app/core/services/product.service";
import { ProductsShareService } from "src/app/core/services/products-data-share.service";
import { Product } from "src/app/core/models/Product";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
  products: Product[];
  searchProducts = [];
  productName: string = "";

  constructor(
    private productsShare: ProductsShareService,
    private productApiService: ProductApiService
  ) {}

  ngOnInit() {
    this.onInitFetchProducts();
    this.onSearchBounce(document.querySelector('input'), 'keyup', 1000);
  }

  onInitFetchProducts() {
    this.productName
      ? this.productsShare.currentProducts.subscribe(
          products => (this.products = products)
        )
      : this.productsShare.changeProducts([], true);
  }

  onSearchBounce(element: any, action: string, delay: number) {
    fromEvent(element, action)
      .pipe(
        debounceTime(delay),
        distinctUntilChanged(),
        switchMap(() =>
          this.productApiService.getProducts(`?name_like=${this.productName}`)
        )
      )
      .subscribe(product => {
        this.searchProducts = product;
        this.onModelChange();
      });
  }

  onModelChange() {
    if (this.productName) {
      if (this.searchProducts.length > 0)
        this.productsShare.changeProducts(this.searchProducts, true);
      else {
        this.productsShare.changeProducts([], true);
        this.searchProducts = [{ name: "No product found!" }];
      }
    } else {
      this.productsShare.changeProducts([], false);
      this.searchProducts = [];
    }
  }

  onSelectSubmit(event) {
    if (this.searchProducts[0].id) {
      this.productName = event.target.innerText;
      this.onSearchBounce(event.target, 'click', 100);
    } else console.log("No product found!");
  }
}
