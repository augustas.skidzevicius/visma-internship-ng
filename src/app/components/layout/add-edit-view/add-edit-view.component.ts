import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { Product } from "src/app/core/models/Product";
import { ImageClass } from "./ImageClass";
import { CustomValidator } from "../../../shared/validators/trim.validator";

@Component({
  selector: "app-add-edit-view",
  templateUrl: "./add-edit-view.component.html",
  styleUrls: ["./add-edit-view.component.scss"]
})
export class AddEditViewComponent implements OnInit {
  @Input() title: string;
  @Input() product: Product;
  @Output() updateProduct: EventEmitter<Product> = new EventEmitter();
  @Output() deleteProduct: EventEmitter<Product> = new EventEmitter();
  @Output() postNewProduct: EventEmitter<Product> = new EventEmitter();

  onSale = false;
  allImages = [
    new ImageClass("ninja-shirt", "Ninja shirt"),
    new ImageClass("ninja-sweater", "Ninja sweater")
  ];

  productForm = new FormGroup({
    id: new FormControl(null),
    name: new FormControl("", [
      Validators.required,
      CustomValidator.trimWhiteSpaces
    ]),
    image: new FormControl(this.allImages[0].imageId, [Validators.required]),
    price: new FormControl(null, [Validators.required]),
    discount: new FormControl(null)
  });

  constructor() {}

  ngOnInit() {
    this.renderEditProduct();
  }

  renderEditProduct() {
    if (!this.product) return;

    this.productForm.patchValue(this.product);
    if (this.product["discount"]) this.onSale = true;
  }

  onSubmit() {
    this.title === "Edit product"
      ? this.updateProduct.emit({
          ...this.productForm.value,
          id: this.product["id"]
        })
      : this.postNewProduct.emit(this.productForm.value);
  }

  onDelete() {
    this.deleteProduct.emit({
      ...this.productForm.value,
      id: this.product["id"]
    });
  }
}
