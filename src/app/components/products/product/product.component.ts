import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';

import { ProductsShareService } from "src/app/core/services/products-data-share.service";
import { Product } from "../../../core/models/Product";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"]
})
export class ProductComponent implements OnInit, OnDestroy {
  products: Product[];
  subscription: Subscription;

  constructor(
    private router: Router,
    private productsShare: ProductsShareService
  ) {}

  ngOnInit(): void {
    this.subscription = this.productsShare.currentProducts.subscribe(products => {
      this.products = products;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getFullPrice(discount: number, price: number): number {
    return discount + price;
  }
}
