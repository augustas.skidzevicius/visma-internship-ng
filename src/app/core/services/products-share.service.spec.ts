import { TestBed } from '@angular/core/testing';

import { ProductsShareService } from './products-data-share.service';

describe('ProductsShareService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductsShareService = TestBed.get(ProductsShareService);
    expect(service).toBeTruthy();
  });
});
