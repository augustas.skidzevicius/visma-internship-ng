import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { ProductApiService } from "../services/product.service";
import { Product } from "../models/Product";

@Injectable({
  providedIn: "root"
})
export class ProductsShareService {
  products: Product[] = [];

  public productsSource = new BehaviorSubject(this.products);
  currentProducts = this.productsSource.asObservable();

  constructor(private productApiService: ProductApiService) {
    this.fetchProducts();
  }

  fetchProducts() {
    this.productApiService.getProducts("").subscribe(products => {
      this.changeProducts(products, true);
    });
  }

  changeProducts(products: Product[], searching: boolean) {
    products.length > 0 && searching
      ? this.productsSource.next(products)
      : this.fetchProducts();
  }
}
