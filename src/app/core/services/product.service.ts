import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

import { Product } from "../models/Product";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({
  providedIn: "root"
})
export class ProductApiService {
  constructor(private http: HttpClient) {}

  // Get products
  getProducts(query: string): Observable<Product[]> {
    return this.http.get<Product[]>(`${environment.productsUrl}${query}`);
  }

  // Get product
  getProduct(id: number): Observable<Product> {
    return this.http.get<Product>(`${environment.productsUrl}?id=${id}`);
  }

  // Delete product
  deleteProduct(product: Product): Observable<Product> {
    return this.http.delete<Product>(
      `${environment.productsUrl}/${product.id}`,
      httpOptions
    );
  }

  // Add product
  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(
      environment.productsUrl,
      product,
      httpOptions
    );
  }

  // Update product
  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(
      `${environment.productsUrl}/${product.id}`,
      product,
      httpOptions
    );
  }
}
