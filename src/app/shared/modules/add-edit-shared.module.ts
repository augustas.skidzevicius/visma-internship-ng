import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AddEditViewComponent } from "src/app/components/layout/add-edit-view/add-edit-view.component";

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule],
  declarations: [AddEditViewComponent],
  exports: [CommonModule, RouterModule, AddEditViewComponent]
})
export class AddEditSharedModule {}
