import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { MainViewComponent } from "src/app/components/layout/main-view/main-view.component";
import { ProductsComponent } from "src/app/components/products/products.component";
import { ProductComponent } from "src/app/components/products/product/product.component";
import { SearchComponent } from 'src/app/components/search/search.component';
import { SortingComponent } from 'src/app/components/sorting/sorting.component';
import { TruncatePipe } from "src/app/shared/pipes/truncate.pipe";

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule],
  declarations: [
    MainViewComponent,
    ProductsComponent,
    ProductComponent,
    TruncatePipe,
    SearchComponent,
    SortingComponent
  ],
  exports: [
    MainViewComponent,
    ProductsComponent,
    ProductComponent,
    TruncatePipe,
    SearchComponent,
    SortingComponent,
    CommonModule,
    RouterModule
  ]
})
export class MainSharedModule {}
