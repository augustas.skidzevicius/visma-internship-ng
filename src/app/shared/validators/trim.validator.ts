import { FormControl, Validators } from "@angular/forms";

export class CustomValidator extends Validators {
  static trimWhiteSpaces(control: FormControl) {
    if (control.value.startsWith(" "))
      return { trimError: { value: "Remove whitespace from the start" } };

    if (control.value.endsWith(" "))
      return { trimError: { value: "Remove whitespace from the end" } };

    return null;
  }
}
